import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

posts = [
  {
    title: 'Mon premier post',
    content: 'Les nouveaux smartphones... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper.',
    loveIts: 0,
    dontLoveIts: 0,
    created_at: new Date('Octobre 17, 2019 10:00:00')
  },
  {
    title: 'Mon deuxième post',
    content: 'Les nouvelles tablettes... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper.',
    loveIts: 0,
    dontLoveIts: 0,
    created_at: new Date('Novembre 18, 2019 11:00:00')
  },
  {
    title: 'Mon trisième post',
    content: 'Du nouveau dans la recherche scientifique... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper.',
    loveIts: 0,
    dontLoveIts: 0,
    created_at: new Date('December 19, 2019 12:00:00')
  }
];
}
